package com.voxloud.provisioning.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.net.URL;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class ProvisioningControllerTest {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;
	
	private final String requestPath = "/api/v1/provisioning/";
	
	private final String invalidMacAddress = "zz-yy-xx-ww-vv-uu";
	
	private final String deskMacAddressWithoutOverrideFragment = "aa-bb-cc-dd-ee-ff";
	
	private final String deskMacAddressWithOverrideFragment = "a1-b2-c3-d4-e5-f6";
	
	private final String conferenceMacAddressWithoutOverrideFragment = "f1-e2-d3-c4-b5-a6";
	
	private final String conferenceMacAddressWithOverrideFragment = "1a-2b-3c-4d-5e-6f";
		
	private StringBuilder provisioningRequestUrlBuilder;
		
	@BeforeEach
	private void before() {
		provisioningRequestUrlBuilder = new StringBuilder("http://localhost:").append(port).append(requestPath);
	}	

	@Test
    public void deskMacAddressWithoutOverrideFragment() throws Exception {
		
		provisioningRequestUrlBuilder.append(deskMacAddressWithoutOverrideFragment);
		
		ResponseEntity<String> response = restTemplate
				.getForEntity(new URL(provisioningRequestUrlBuilder.toString()).toString(), String.class);

		assertEquals(HttpStatus.OK, response.getStatusCode());
		
		//Check for the fields from the override fragment
		assertFalse(response.getBody().contains("timeout=10"));
		assertTrue(response.getBody().contains("port=5060"));		
    }

	@Test
    public void deskMacAddressWithOverrideFragment() throws Exception {
		
		provisioningRequestUrlBuilder.append(deskMacAddressWithOverrideFragment);
		
		ResponseEntity<String> response = restTemplate
				.getForEntity(new URL(provisioningRequestUrlBuilder.toString()).toString(), String.class);

		assertEquals(HttpStatus.OK, response.getStatusCode());
		
		//Check for the fields from the override fragment
		assertTrue(response.getBody().contains("timeout=10"));
		assertTrue(response.getBody().contains("port=5161"));		
    }

	@Test
    public void conferenceMacAddressWithoutOverrideFragment() throws Exception {
		
		provisioningRequestUrlBuilder.append(conferenceMacAddressWithoutOverrideFragment);
		
		ResponseEntity<String> response = restTemplate
				.getForEntity(new URL(provisioningRequestUrlBuilder.toString()).toString(), String.class);

		assertEquals(HttpStatus.OK, response.getStatusCode());
		
		//Check for the fields from the override fragment
		assertFalse(response.getBody().contains("timeout=10"));
		assertTrue(response.getBody().contains("\"port\":\"5060\""));		
    }
	
	@Test
    public void conferenceMacAddressWithOverrideFragment() throws Exception {
		
		provisioningRequestUrlBuilder.append(conferenceMacAddressWithOverrideFragment);
		
		ResponseEntity<String> response = restTemplate
				.getForEntity(new URL(provisioningRequestUrlBuilder.toString()).toString(), String.class);

		assertEquals(HttpStatus.OK, response.getStatusCode());
		
		//Check for the fields from the override fragment
		assertTrue(response.getBody().contains("\"timeout\":10"));
		assertTrue(response.getBody().contains("\"port\":\"5161\""));				
    }
	
	@Test
	public void macAddressNotFound() throws Exception {

		provisioningRequestUrlBuilder.append(invalidMacAddress);
		
		ResponseEntity<String> response = restTemplate
				.getForEntity(new URL(provisioningRequestUrlBuilder.toString()).toString(), String.class);

		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}

}
