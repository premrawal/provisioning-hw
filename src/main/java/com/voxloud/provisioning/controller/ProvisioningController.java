package com.voxloud.provisioning.controller;

import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.voxloud.provisioning.service.ProvisioningService;

@RestController
@RequestMapping("/api/v1")
public class ProvisioningController {

	private ProvisioningService provisioningService;
	
	public ProvisioningController(ProvisioningService provisioningService) {
		this.provisioningService = provisioningService;
	}
	
	@GetMapping("/provisioning/{macAddress}")
	public ResponseEntity<String> getProvisioningConfiguration(@PathVariable String macAddress) {
		
		String configuration = provisioningService.getProvisioningFile(macAddress);
		
		if(StringUtils.hasLength(configuration)) {
			return ResponseEntity.ok(configuration);
		} else {
			return ResponseEntity.of(Optional.empty());
		}
		
	}
	
	
}