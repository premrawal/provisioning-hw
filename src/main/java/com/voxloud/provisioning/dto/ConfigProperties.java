package com.voxloud.provisioning.dto;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "provisioning")
public class ConfigProperties {

    private String domain;

    private String port;

    private String codecs;
    
}