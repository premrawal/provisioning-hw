package com.voxloud.provisioning.service;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.voxloud.provisioning.dto.ConfigProperties;
import com.voxloud.provisioning.entity.Device;
import com.voxloud.provisioning.entity.Device.DeviceModel;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ConferenceConfigurationGenerator implements ConfigurationGenerator {

	@Autowired
	ConfigProperties configProperties;

	@Override
	public DeviceModel getType() {
		return DeviceModel.CONFERENCE;
	}
	
	@Override
	public String generateConfiguration(Device device) {

		Map<String, String> configurationMap = new LinkedHashMap<>();
				
		// Store the device configuration in the map
		configurationMap.put("username", device.getUsername());
		configurationMap.put("password",device.getPassword());
		
		// store the configProperties in the map
		configurationMap.put("domain",configProperties.getDomain());
		configurationMap.put("port",configProperties.getPort());
		configurationMap.put("codecs",configProperties.getCodecs());
		
		// Check if the overrideFragment is present.
		if(StringUtils.hasLength(device.getOverrideFragment())) { // store the overrideFragment values in the map
			configurationMap.putAll(getOverrideFragment(device.getOverrideFragment()));
		}
		
		return converttoJsonString(configurationMap);
	}	
	
	private Map<String, String> getOverrideFragment(String overrideFragment) {

		Map<String, String> overrideFragmentMap = new HashMap<>();
		
		try {
			ObjectMapper mapper = new ObjectMapper();
			overrideFragmentMap.putAll(mapper.readValue(overrideFragment, Map.class));
		} catch (JsonProcessingException jpe) {
			log.error("Error occurred while parsing overrideFragment {}", overrideFragment, jpe);			
		}

		return overrideFragmentMap;		
	}
	
	private String converttoJsonString(Map<String, String> configurationMap) {
		
		String configurationJson = "";
		
		try {
			ObjectMapper mapper = new ObjectMapper();
			configurationJson = mapper.writeValueAsString(configurationMap);
		} catch (JsonProcessingException jpe) {
			log.error("Error occurred while converting configuratonMap to Json ", jpe);			
		}
		
	    return configurationJson;
	}

}
