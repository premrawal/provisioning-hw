package com.voxloud.provisioning.service;

import com.voxloud.provisioning.entity.Device;
import com.voxloud.provisioning.entity.Device.DeviceModel;

public interface ConfigurationGenerator {

	DeviceModel getType();
	String generateConfiguration(Device device);
	
}
