package com.voxloud.provisioning.service;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.voxloud.provisioning.entity.Device;
import com.voxloud.provisioning.repository.DeviceRepository;

@Service
public class ProvisioningServiceImpl implements ProvisioningService {

	private DeviceRepository deviceRepository;

	private ConfigurationGeneratorFactory configurationGeneratorFactory;

	public ProvisioningServiceImpl(DeviceRepository deviceRepository, ConfigurationGeneratorFactory configurationGeneratorFactory) {
		this.deviceRepository = deviceRepository;
		this.configurationGeneratorFactory = configurationGeneratorFactory;
	}

	public String getProvisioningFile(String macAddress) {

		// Generate String in form of text or JSON, also consider override fragment
		Optional<Device> oDevice = this.deviceRepository.findById(macAddress);

		if (oDevice.isEmpty()) {// Return empty String if macAddress is not found.
			return "";
		}

		return generateConfigurationForDevice(oDevice.get());
	}

	private String generateConfigurationForDevice(Device device) {
    			
		ConfigurationGenerator configurationGenerator = configurationGeneratorFactory.getConfigurationGenerator(device.getModel());
			    	
    	return configurationGenerator.generateConfiguration(device);
    	
    }

}
