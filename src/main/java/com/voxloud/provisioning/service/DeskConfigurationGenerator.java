package com.voxloud.provisioning.service;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.voxloud.provisioning.dto.ConfigProperties;
import com.voxloud.provisioning.entity.Device;
import com.voxloud.provisioning.entity.Device.DeviceModel;

@Component
public class DeskConfigurationGenerator implements ConfigurationGenerator {

	@Autowired
	ConfigProperties configProperties;

	@Override
	public DeviceModel getType() {
		return DeviceModel.DESK;
	}
	
	@Override
	public String generateConfiguration(Device device) {

		Map<String, String> configurationMap = new LinkedHashMap<>();
				
		// Store the device configuration in the map
		configurationMap.put("username", device.getUsername());
		configurationMap.put("password",device.getPassword());
		
		// store the configProperties in the Map
		configurationMap.put("domain",configProperties.getDomain());
		configurationMap.put("port",configProperties.getPort());
		configurationMap.put("codecs",configProperties.getCodecs());
		
		// Check if the overrideFragment is present.
		if(StringUtils.hasLength(device.getOverrideFragment())) {// store the overrideFragment values in the map
			configurationMap.putAll(getOverrideFragment(device.getOverrideFragment()));
		} 
			
		return converttoPropertyString(configurationMap);
	}
	
	
	private Map<String, String> getOverrideFragment(String overrideFragment) {
		
		Map<String, String> overrideFragmentMap = new HashMap<>();			
		
		String[] fragmentProperties = overrideFragment.split("\n");
		
		for(String fragmentProperty : fragmentProperties) {
			String[] fragments = fragmentProperty.split("=");
			overrideFragmentMap.put(fragments[0], fragments[1]);
		}
		
		return overrideFragmentMap;
	}
	
	private String converttoPropertyString(Map<String, String> configurationMap) {
		
	    String configurationProps = 
	    		configurationMap.entrySet().stream()
	    			.map(entry -> entry.getKey() + "=" + entry.getValue())
	    			.collect(Collectors.joining("\n"));
	    
	    return configurationProps;
	}
	
}
