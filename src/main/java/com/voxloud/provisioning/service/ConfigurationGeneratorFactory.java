package com.voxloud.provisioning.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.springframework.stereotype.Service;

import com.voxloud.provisioning.entity.Device.DeviceModel;

@Service
public class ConfigurationGeneratorFactory {

	private final Map<DeviceModel, ConfigurationGenerator> generatorCache = new HashMap<>();
	
	private ConfigurationGeneratorFactory(List<ConfigurationGenerator> configurationGenerators) {
		configurationGenerators.stream().forEach(
				configurationGenerator -> {
					generatorCache.put(configurationGenerator.getType(), configurationGenerator);
		});
	}
		
	
	public ConfigurationGenerator getConfigurationGenerator(DeviceModel model) {

		ConfigurationGenerator configurationGenerator = generatorCache.get(model);
		
		if(Objects.isNull(configurationGenerator)) {
			throw new IllegalArgumentException("Unsupported model type");
		}
		
		return configurationGenerator;		
	}
	
	
}
